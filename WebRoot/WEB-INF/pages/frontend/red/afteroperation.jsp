<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/common/default/css/errorform.css" />

<div class="w" id="regist">
	<div id="mq" style="width: 650px;margin-left: 320px;height: 342px;margin-top: 20px;">
        <i class="reg-success"></i>

        <div class="reg-tips-info reg-tips-info-suc"> 恭喜，303353094@qq.com 已注册成功！</div>
        <div class="reg-btn-box">
            <a class="reg-btn-small" href="http://www.jd.com/">立即购物</a>
        </div>
        <div class="reg-nickname-tips">
            您的昵称：<em class="reg-nickname" id="orgNick">303353094</em> 会展示在页面顶部和商品评价等地方，如不希望暴露，建议您：<a class="emreg-nickname" id="changeNickname" href="#">修改昵称</a>
        </div>
        <div class="reg-nickname-revise hide">
            <label class="fl">您的昵称：</label>

            <div class="fl">
                <input value="303353094" class="text" id="nicknameInput" type="text">
                <input value="保存" class="reg-btn3 j_save" type="submit">
            </div>
        </div>
        <div class="reg-form">
            <p class="reg-form-tips">超过<em>80%</em>的用户选择了立即验证邮箱，账户更安全购物更放心。</p>


            <div class="reg-email-hidden mt20" id="sendEmailDiv">
                <label>您的邮箱：</label>

                                    <input value="303353094@qq.com" id="emailStr" autocomplete="off" type="text">
                
                <input class="reg-btn2" value="发送验证邮件" id="sendEmail" type="button">
                <span class="clr"></span>

                <div id="email_error" style="color: #FF0000;padding-left:63px;"></div>
                <div id="email_focus" style="padding-left:63px;"></div>
            </div>

            <div class="check-email mt20" id="sendEmailSuccessDiv">
                <p>系统已向您的邮箱 <em id="newEmail">303353094@qq.com</em> 发送了一封验证邮件，请您登录邮箱，点击邮件中的链接完成邮箱验证。如果您超过2分钟未收到邮件，您可以<a class="j_resend" href="javascript:void(0);" onclick="reSendEmail();">重新发送</a>
                                    </p>
                <a href="http://mail.qq.com/" class="reg-btn4" id="emailLogin" style="color: #FFFFFF;">登录邮箱</a>
                <span class="check-email-suc-hidden" id="reSendEmailSuccess">验证邮件已重新发送</span></div>
        </div>
    </div>
</div>   
<script type="text/javascript">
	 function turnToIndex(){
		window.location.href = "${pageContext.request.contextPath}/index";
	 }
</script>