<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/common/js/randomimage.js'/>"></script>
<script type="text/javascript">
		 		//used by reg.js
				var contextPath = '${pageContext.request.contextPath}';
				var subTab = '<%=request.getParameter("t") %>';
				var userName = '<%=request.getParameter("userName")%>';
				var code = '<%=request.getParameter("code")%>';
				
				if(subTab != 'null'){
					window.location.href = contextPath + "/setNewPwd/"+userName+"/"+code;
				}
</script>
<!--nav-->
 <div class="nav">
   <div class="wrap">
   <p class="dengl">
   <span class="yongh">找回密码</span>
   </p>
   </div> 
 </div>
<!--nav end-->

<div class="w"> 
    <div class="login_left wrap">
      <div class="news_wrap">
         <div class="news_bor" id="entry">
	         <div class="mc" style="height: 233px;" >
					<div id="step">
			            <ul>
			               <li class="cur">填写账户名</li>
			               <li class="">验证身份</li>
			               <li class="">设置新密码</li>
			               <li class="four">完成</li>
			            </ul>
			        </div>
					<div class="form">
						<div class="item">
							<span class="label">账户名：</span>
							<div class="fl">
								<!-- text_error -->
								<input tabindex="1" class="text text-color"  id="name" name="name" type="text" autocomplete="off" onfocus="setfocus();" onblur="setblur();">
								<span class="clr"></span>
								<label id="name_error" calss=""></label>
							</div>
						</div>
						<div class="item">
							<span class="label">验证码：</span>
							<div class="fl">
									<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
									<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
									<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
									<input class="text" tabindex="2" name="randNum" id="randNum" type="text" autocomplete="off" maxlength="4" style="width: 100px" onfocus="randNumSetFocus();" onblur="randNumSetBlur();">
									<label><img id="randImage" name="randImage" src="<ls:templateResource item='/captcha.svl'/>"  style="vertical-align: middle;"/>看不清？
            						<a href="javascript:void(0)" onclick="javascript:changeRandImg('${pageContext.request.contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            						</label>
								<span class="clr"></span>
								<label id="authCode_error" class="msg-error"></label>
							</div>
						</div>
						<div class="item">
							<span class="label">&nbsp;</span>
							<input tabindex="4" value="下一步" onclick="doIndex();" id="findPwdSubmit" class="btn-img btn-entry" type="button">
						</div>
					</div>

			<span class="clr"></span>
		
			</div>   
        </div>      
      </div>                                  
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
</div>        

<script type="text/javascript">
	/**检查是否为手机号码形式**/
	function isPhone(str){
		var mobile = /^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\d{8}$/;
		return mobile.test(str);
	}
	
	/**检查手机号码是否存在**/
	function checkPhone() {
		var result = true;
		var userPhoneValue = jQuery("#name").val();
		$.ajax({
				url: contextPath + "/isPhoneExist", 
				data: {"Phone":userPhoneValue},
				type:'post', 
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					 //console.log(textStatus, errorThrown);
					},
				success:function(retData){
					if('true' == retData){
						result = false;
					}
				}
			});
		return result;
	}
	
	/*** 检查是否由数字字母和下划线组成 ***/
	String.prototype.isAlpha = function() {
		return (this.replace(/\w/g, "").length == 0);
	}
	/**检查用户名是否存在**/
	function checkName() {
		var result = true;
		var nameValue = jQuery("#name").val();
	    $.ajax({
			url: contextPath + "/isUserExist", 
			data: {"userName":nameValue},
			type:'post', 
			async : false, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
			 		 //console.log(textStatus, errorThrown);
			},
			success:function(retData){
				 if('true' == retData){
					result = false;
					}
			}
		});
    return result;
	}
	
	/**检查是否为邮箱形式**/
	function isEmail(str){
		var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		return reg.test(str);
	}
	/**检查邮箱是否存在**/	
	function checkEmail() {
		var result = true;
		var userMailValue = jQuery("#name").val();
		$.ajax({
			url: contextPath + "/isEmailExist", 
			data: {"email":userMailValue},
			type:'post', 
			async : false, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			},
			success:function(retData){
				if('true' == retData){
					result = false;
				}
			}
		});
		return result;
	}

	function setfocus(){
		$("#name").attr("class","text highlight1");
		$("#name_error").show();
		$("#name_error").attr("class","msg-text");
		$("#name_error").html("请输入您的用户名/邮箱/已验证手机");
	}
	
	function setblur(){
		var name = $("#name").val();
		if(name.length!=0){
			if(name.length<4){
				$("#name").attr("class","text highlight2");
				$("#name_error").show();
				$("#name_error").attr("class","msg-error");
				$("#name_error").html("不能小于4个字符");
			}else if(isEmail(name)){
				if(checkEmail()){
					$("#name").attr("class","text highlight2");
					$("#name_error").show();
					$("#name_error").attr("class","msg-error");
					$("#name_error").html("此邮箱不存在！");
				}else{
					$("#name").attr("class","text");
					$("#name_error").hide();
				}
			}else if(isPhone(name)){
				if(checkPhone()){
					$("#name").attr("class","text highlight2");
					$("#name_error").show();
					$("#name_error").attr("class","msg-error");
					$("#name_error").html("此手机号码不存在！");
				}else{
					$("#name").attr("class","text");
					$("#name_error").hide();
				}
			}else if(name.isAlpha()){
				if(checkName()){
					$("#name").attr("class","text highlight2");
					$("#name_error").show();
					$("#name_error").attr("class","msg-error");
					$("#name_error").html("此用户名不存在！");
				}else{
					$("#name").attr("class","text");
					$("#name_error").hide();
				}
			}
		}else{
			$("#name").attr("class","text");
			$("#name_error").hide();
		}
	}
	
	function randNumSetFocus(){
		$("#randNum").attr("class","text highlight1");
	}
	
	function randNumSetBlur(){
		$("#randNum").attr("class","text");
	}
	
	function doIndex(){
		if($("#name").val().length ==0){
			$("#name").attr("class","text highlight2");
			$("#name_error").show();
			$("#name_error").attr("class","msg-error");
			$("#name_error").html("请填写您的用户名/邮箱/已验证手机");
		}else{
		  if(validateRandNum('${pageContext.request.contextPath}')){
			$.ajax({
				url:'${pageContext.request.contextPath}/proveStatus', 
				data:{"name":$("#name").val()},
				type:'post', 
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
				  $("#entry").html(result);
				}
				});
		   }
	   }
	}
	
	function selectVerifyType(obj){
		if(obj=="email"){
			$("#emailDiv").show();
			$("#mobileDiv").hide();
		}else{
			$("#mobileDiv").show();
			$("#emailDiv").hide();
		}
	}
	
	//验证用户输入的验证码是否正确
	function verifySMSCode(){
		var verify = false;
		var name = $("#userName").text();
		$.ajax({
			url:'${pageContext.request.contextPath}/verifySMSCode', 
			data:{"userName":name,"code":$("#code").val()},
			type:'post', 
			async : false, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
			},
			success:function(result){
				verify = result;
			}
			});
		return verify;
	}
	
	function codeFocus(){
	    $("#code").attr("class","text text-1 highlight1");
	    $("#code_error").hide();
	}
	
	function codeBlur(){
		var codeValue = $("#code").val();
		if(codeValue.length == 0 || codeValue.length < 6){
			$("#code").attr("class","text text-1 highlight2");
			$("#code_error").show();
			$("#code_error").attr("class","msg-error");
			$("#code_error").html("验证码错误");
		}else{
			$("#code").attr("class","text text-1");
			$("#code_error").hide();
		}
	}
	
	//去往修改密码页面
	function validFindPwdCode(){
		if(verifySMSCode()){
			$.ajax({
				url:'${pageContext.request.contextPath}/changePwd', 
				type:'post', 
				data:{"userName":$("#userName").text(),"code":$("#code").val()},
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
				  $("#entry").html(result);
				}
				});
		}else{
			$("#code").attr("class","text text-1 highlight2");
			$("#code_error").show();
			$("#code_error").attr("class","msg-error");
			$("#code_error").html("验证码错误");
		}
	}
	
	function updatePassword(){
		var rpwValue = $("#repassword").val();
		var pwValue = $("#password").val();
		var pwError = $("#password_error");
		var rpwError = $("#repassword_error");
		if(pwValue.length ==0){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("请设置新密码");
		}else if(pwValue.length < 6){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("密码长度不正确，请重新设置");
		}else if(rpwValue.length ==0){
			$("#repassword").attr("class","text highlight2");
			rpwError.show();
			rpwError.attr("class","msg-error");
			rpwError.text("请确认新密码");
		}else if(rpwValue != pwValue){
			$("#repassword").attr("class","text highlight2");
			rpwError.show();
			rpwError.attr("class","msg-error");
			rpwError.text("两次输入的密码不一致，请重新输入");
		}else{
			$.ajax({
				url:'${pageContext.request.contextPath}/success', 
				type:'post', 
				data:{"newPassword":$("#password").val(),"userName":$("#userName").val(),"code":$("#code").val()},
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
				  $("#entry").html(result);
				}
				});
		}
	}

</script>