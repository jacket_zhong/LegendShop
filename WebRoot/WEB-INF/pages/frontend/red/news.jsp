<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<!----blue两栏---->
<div class="w"> 
  <div class="clear"></div>
 </div>

<div class="w"> 
    <!----右边---->
       <%@ include file="newsCategory.jsp"%>
    <!----右边end---->
    <!----左边---->
    <div class="index_left">
      <div class="news_wrap">
         
         <div class="news_bor">
         
            <h3 class="tit">${news.newsTitle}</h3>
            <div class="nge"></div>
            <p class="author">作者: ${news.userName} <fmt:formatDate value="${news.newsDate}" pattern="yyyy-MM-dd hh:MM"/>  来源: 速途网我要评论 (2) 访问次数 12408</p>
            <c:if test="${ not empty news.newsBrief}">
            	<p class="abstract"><strong>摘要：</strong>${news.newsBrief}.......</p>
            </c:if>
            <div class="newscontent">
              ${news.newsContent}
           </div>
         
             <div class="tishi">
               <c:forEach items="${requestScope.newsList}" var="news" varStatus="status">
               		<p><a href="${pageContext.request.contextPath}/news/${news.newsId}">
               		<c:if test="${status.first}">上一篇:</c:if>
               		<c:if test="${status.last}">下一篇：</c:if>${news.newsTitle}
               			</a>
               		</p>
               </c:forEach>           
             </div>
   			</br>
           <div class="pinglun">

           </div>
        </div>
     
       </div>      
     
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
 </div>
<!----red两栏end---->

<!----advtisementend---->
<!----green两栏---->
<div class="w"> 
    <!----右边---->
    <!----右边end---->
    
    <!----左边---->
    <!----左边end---->
    
  <div class="clear"></div>
 </div>
<!----green两栏end---->


<!----down两栏---->
 <div class="w"> 
    <!----右边---->
    <!----右边end---->
    
    <!----左边---->
    <!----左边end---->
   <div class="clear"></div>
 </div>
<!----down两栏end---->