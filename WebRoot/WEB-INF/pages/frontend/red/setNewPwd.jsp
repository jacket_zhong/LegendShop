<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<!--nav-->
 <div class="nav">
   <div class="wrap">
   <p class="dengl">
   <span class="yongh">找回密码</span>
   </p>
   </div> 
 </div>
<!--nav end-->

<div class="w"> 
    <div class="login_left wrap">
      <div class="news_wrap">
         <div class="news_bor" id="entry">
         	<div class="mc">
		<div id="step">
            <ul>
               <li class="one one1">填写账户名</li>
               <li class="two">验证身份</li>
               <li class="three cur">设置新密码</li>
               <li class="four">完成</li>
            </ul>
        </div>
		<div class="form">
		
            <div class="item" style="display:none">
				<span class="label">历史收货人手机号码：</span>
				<div class="fl">
					<input tabindex="1" onfocus="mobileFocus();" onblur="mobileBlur();" class="text " id="mobile" type="text">
					<label class="blank invisible" id="username_succeed"></label>
					<span class="clr"></span>
					<label id="mobile_error" class=""></label>
				</div>
			</div>
			
			<input class="text" id="code" name="code" type="hidden" value="${code}"/>
			<input class="text" id="userName" name="userName" type="hidden" value="${userName}"/>
			<div class="item">
				<span class="label">新登录密码：</span>
				<div class="fl">
					<input value="" tabindex="1" class="text" id="password" type="password" maxlength="18">
					<span class="clr"></span>
					<label id="pwdstrength" class="hide" ><span class="fl">安全程度：</span><b></b></label>
					<span class="clr"></span>
					<label id="password_error" class=""></label>
					<span class="clr"></span>
				</div>
			</div>
			<div class="item">
				<span class="label">确认新密码：</span>
				<div class="fl">
					<input value="" tabindex="2" class="text" id="repassword" type="password" maxlength="18">
					<span class="clr"></span>
					<label id="repassword_error" class=""></label>
				</div>
			</div>
			<div class="item">
				<span class="label">&nbsp;</span>
				<input tabindex="8" value="提交" id="resetPwdSubmit" onclick="updatePassword();" class="btn-img btn-entry" type="button">
			</div>
		</div>
		<!--[if !ie]>form end<![endif]-->
		<span class="clr"></span>
	</div>
         
         </div>      
      </div>                                  
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
</div>        

<script type="text/javascript">
	jQuery(document).ready(function() {
		$("#password").bind('keyup onfocus onblur', function () {
			  $("#pwdstrength").show();
			  var index = checkStrong(this.value);
			  $("#pwdstrength").removeClass("strengthA");
			  $("#pwdstrength").removeClass("strengthB");
			  $("#pwdstrength").removeClass("strengthC");
			  var  letter = ( index == 0? 0:index == 1? 'A':index == 2? 'B':'C') ;
			  if(letter==0){
				  $("#pwdstrength").hide();
			  }
			$("#pwdstrength").attr("class","strength" + letter );
		});
		
		//密码检测密码强度
		function checkStrong(sValue) {
		    var modes = 0;
		    //正则表达式验证符合要求的
		    if (sValue.length < 1) return modes;
		    if (/\d/.test(sValue)) modes++; //数字
		    if (/[a-z]/.test(sValue)) modes++; //小写
		    if (/[A-Z]/.test(sValue)) modes++; //大写  
		    if (/\W/.test(sValue)) modes++; //特殊字符
		   
		   //逻辑处理
		    switch (modes) {
		        case 1:
		            return 1;
		            break;
		        case 2:
		            return 2;
		        case 3:
		        case 4:
		            return sValue.length < 12 ? 3 : 4;
		            break;
		    }
		}

	});
	
	$("#password").bind('focus', function () {
		//var pwError = $("#password_error");
		 $("#password").attr("class","text highlight1");
		 $("#password_error").hide();
		 //pwError.attr("class","msg-text");
		 //pwError.text("由字母加数字或符号至少两种以上字符组成的6-20位半角字符，区分大小写。");
	});
	
	$("#password").bind('blur', function () {
		var pwValue = $("#password").val();
		var error = $("#password_error");
		 if(pwValue.length>0 && pwValue.length<6){
			 $("#password").attr("class","text highlight2");
			 error.show();
			 error.attr("class","msg-error");
			 error.text("密码太弱，有被盗风险，请设置由多种字符组成的复杂密码");
		 }else{
			 $("#password").attr("class","text");
			 error.hide();
		 }
	});
	
	$("#repassword").bind('focus',function(){
		$("#repassword").attr("class","text highlight1");
	    $("#repassword_error").hide();
	});
	
	$("#repassword").bind('blur', function () {
		var rpwValue = $("#repassword").val();
		var pwValue = $("#password").val();
		var error = $("#repassword_error");
		if(rpwValue.length != 0 && rpwValue != pwValue){
				$("#repassword").attr("class","text highlight2");
				error.show();
				error.attr("class","msg-error");
				error.text("两次输入的密码不一致，请重新输入");
		}else{
			$("#repassword").attr("class","text");
			error.hide();
		}
	});

	function updatePassword(){
		var rpwValue = $("#repassword").val();
		var pwValue = $("#password").val();
		var pwError = $("#password_error");
		var rpwError = $("#repassword_error");
		if(pwValue.length ==0){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("请设置新密码");
		}else if(pwValue.length < 6){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("密码长度不正确，请重新设置");
		}else if(rpwValue.length ==0){
			$("#repassword").attr("class","text highlight2");
			rpwError.show();
			rpwError.attr("class","msg-error");
			rpwError.text("请确认新密码");
		}else if(rpwValue != pwValue){
			$("#repassword").attr("class","text highlight2");
			rpwError.show();
			rpwError.attr("class","msg-error");
			rpwError.text("两次输入的密码不一致，请重新输入");
		}else{
			$.ajax({
				url:'${pageContext.request.contextPath}/success', 
				type:'post', 
				data:{"newPassword":$("#password").val(),"userName":$("#userName").val(),"code":$("#code").val()},
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
				  $("#entry").html(result);
				}
				});
		}
	}
	</script>