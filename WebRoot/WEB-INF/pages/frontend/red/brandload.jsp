<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
		<div class="mc brandslist">
				<ul class="list-h">
				<div align="center">
					<c:forEach items="${requestScope.brandList }" var="brand">
							<li>
							  <div>
									<c:choose>
										<c:when test="${brand.brandPic == null}">
											<span class="b-img">
												<a href="#" ><img width="138" height="46" src="../common/red/images/brandicon.jpg" /></a>
											</span>
										</c:when>
										<c:otherwise>
											<span class="b-img">
												<a href="#" ><img width="138" height="46" src="<ls:photo item='${brand.brandPic}'/>" /></a>
											</span>
										</c:otherwise>
									</c:choose>
									<span class="b-name">
										<a href="#" >${brand.brandName}</a>
									</span>
							</div>
							</li>
					</c:forEach>
					</div>
       <div class="clear"></div>
       <c:if test="${not empty requestScope.moreBrandList }">
	       <div class="extra">
	       <strong>更多品牌：</strong>
	       <c:forEach items="${requestScope.moreBrandList }" var="morebrand">
	       		<a href="#" target="_blank">${morebrand.brandName}</a>
	       </c:forEach>
	      </div>    
	</c:if>
