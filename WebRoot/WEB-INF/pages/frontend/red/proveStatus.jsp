<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<div class="mc" style="height: 353px;">
		<div id="step">
            <ul>
               <li class="one">填写账户名</li>
               <li class="two cur">验证身份</li>
               <li class="">设置新密码</li>
               <li class="four">完成</li>
            </ul>
        </div>
		<div class="form">
			<div class="item">
				<span class="label">请选择验证身份方式：</span>
				<div class="fl">
				     <select class="selected" id="type" onchange="selectVerifyType(this.options[selectedIndex].value);">
						  <option value="mobile">已验证手机</option>
						  <option value="email">邮箱</option>
					 </select>
				</div>
			</div>
			<div id="mobileDiv">
        			<div class="item">
						<span class="label">昵称：</span>
        				<div class="fl">
							<label class="text-ifo text-two">${userInfo.userName}</label>
        					<label class="blank invisible" id="username_succeed"></label>
        					<span class="clr"></span>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">已验证手机：</span>
        				<div class="fl">
        					<div id="sendMobileCodeDiv"><label class="text-ifo text-one" id ="userMobile">${userInfo.userMobile}</label><a class="e-ifon" href="javascript:void(0);" id="sendMobileCode">获取短信验证码</a></div>
							<div id="timeDiv" style="display:none"><label class="text-ifo text-one">${userInfo.userMobile}</label><a class="e-time" href="javascript:void(0);" id="sendMobileCode"><strong id="timer" class="ftx-01"></strong>秒后重新发送</a><label>验证码已发送，请查收短信!</label></div>
        					<span class="clr"></span>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">短信验证码：</span>
        				<div class="fl">
        					<input tabindex="6" maxlength="6" onfocus="codeFocus();" onblur="codeBlur();" disabled="disabled" class="text text-1" id="code" type="text">
							<span class="clr"></span>
							<label id="code_error" class="msg-error"></label>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">&nbsp;</span>
        				<input tabindex="8" value="下一步" id="submitCode" onclick="validFindPwdCode();" class="btn-img btn-entry" type="button">
        			</div>
          </div>
		<div id="emailDiv" style="display:none">
        			<div class="item">
						<span class="label">昵称：</span>
        				<div class="fl">
							<label class="text-ifo text-two" id="userName">${userInfo.userName}</label>
        					<label class="blank invisible" id="username_succeed"></label>
        					<span class="clr"></span>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">邮箱地址：</span>
        				<div class="fl">
        					<label class="text-ifo text-two">${userInfo.userMail}</label>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">&nbsp;</span>
        				<a class="e-ifon" href="javascript:void(0);" onclick="sendFindPwdEmail()" id="sendMail">发送验证邮件</a>
        			</div>
    			</div>
    				</div>
		<!--[if !ie]>form end<![endif]-->
		<span class="clr"></span>
</div>
<script type="text/javascript">
var wait;
var timer = null;
var interVal = 60;
$(document).ready(function() {
	var phoneVerifn = ${userInfo.phoneVerifn};
	var mailVerifn = ${userInfo.mailVerifn};
	var pVoption = "<option values='mobile'>已验证手机</option>";
	var mVoption = "<option values='email'>邮箱</option>";
	if(phoneVerifn != 1 || mailVerifn != 1){
		if(phoneVerifn == 1){
			$("#type").html(pVoption);
		}else{
			$("#mobileDiv").remove(); 
		}
		
		if(mailVerifn == 1){
			$("#type").html(mVoption);
			$("#emailDiv").show();
		}else{
			$("#emailDiv").remove(); 
		}
	}

	$("#sendMobileCode").bind("click",function(){
		timer = sendSMSCode();
	});
	
	calRemainTime();
});

//倒计时, need cookies
function onTimer(remains) {
	var sendMobileCode = $("#sendMobileCode");
	if (remains == 0) {
		$("#timer").html(0);
		$("#sendMobileCodeDiv").show();
		$("#timeDiv").hide();
		$("#sendMobileCode").unbind("click").bind("click",function(){ sendSMSCode(); });
		wait = interVal;
		window.clearTimeout(timer);
	} else {
		$("#sendMobileCodeDiv").hide();
		$("#timeDiv").show();
		$("#timer").html(wait);
		var remains = wait--;
		timer = setTimeout("onTimer(" + remains +")",1000);
	}
}

function calRemainTime(){
  if(wait!= null && wait !=interVal){
	   	var sendMobileCode = $("#sendMobileCode");
		$("#sendMobileCodeDiv").hide();
		$("#timeDiv").show();
		$("#code").removeAttr("disabled");
		$("#sendMobileCode").unbind("click");
		$("#timer").html(wait);
	}else{
		wait = interVal;
	}
}

//发送验证短信
function sendSMSCode(){
	var timer;
	var name = $("#userName").text();
	var phone = $("#userMobile").text();
	$.ajax({
		url:"${pageContext.request.contextPath}/sendSMSCode", 
		data:{"userName":name,"mobile":phone},
		type:'post', 
		dataType : 'json', 
		async : false,   
		error: function(jqXHR, textStatus, errorThrown) {
	 		alert(textStatus, errorThrown);
		},
		success:function(retData){
			var sendMobileCode = $("#sendMobileCode");
			sendMobileCode.unbind("click");
			if(retData == 'OK'){
	      			 $("#code").removeAttr("disabled");
	      			$("#sendMobileCodeDiv").hide();
	      			 $("#timeDiv").show();
	      			 //调用倒计时
					 timer = setTimeout("onTimer(" + interVal + ")",1000);
			}else{
					  $("#sendMobileCodeDiv").show();
	      			 $("#timeDiv").hide();
					  var code_error = $("#code_error");
					  code_error.html("发送短信频率过高，请稍等再试试");
					  code_error.show();
	      			  $("#code").attr("disabled",true); 
			}
		}
		});	
	return timer;
}

//发送验证邮件
function sendFindPwdEmail(){
	var name = '${userInfo.userName}';
	alert(name);
	$.ajax({
		url:"${pageContext.request.contextPath}/confirmationMail",
		data:{"userName": name},
		type:'post',  
		async : true,   
		error: function(jqXHR, textStatus, errorThrown) {
	 		alert(textStatus, errorThrown);
		},
		success:function(retData){
			 $("#entry").html(retData);
		}
		});	
}

</script>